CREATE DATABASE IF NOT EXISTS uisaux;
use uisaux;

create table usuarios(
codigo int(10) not null,
nombres varchar(20) not null,
apellidos varchar(20) not null,
tipo_de_actor varchar(20) not null,
contrasena varchar(20) not null,
primary key (codigo)
);

create table auxiliaturas(
codigo int NOT NULL AUTO_INCREMENT,
fecha_inicio Datetime not null,
Administrativo_creador int(10) not null,
estado varchar(20) not null,
texto varchar(400) not null,
auxiliar int(10) null,
fecha_finalizacion Datetime null,
primary key (codigo),
FOREIGN KEY (Administrativo_creador) REFERENCES usuarios(codigo),
FOREIGN KEY (auxiliar) REFERENCES usuarios(codigo)
);

create table postulaciones(
codigo int NOT NULL AUTO_INCREMENT,
aspirante int(10) not null,
fecha_postulacion Datetime not null,
fecha_dada_baja Datetime null,
estado varchar(20) not null,
auxiliatura int not null,
primary key (codigo),
FOREIGN KEY (aspirante) REFERENCES usuarios(codigo),
FOREIGN KEY (auxiliatura) REFERENCES auxiliaturas(codigo)
);

#tipo de actor:
# S = eStudiante
# A = Administrativo
# D = aDministrador
# E = Ejecutor

INSERT INTO usuarios
VALUES (1100001,'Jose Nelson','Amaris Ortiz','S','estudiante01'),
		(1100002,'Sergio Andres','Dulcey','S','estudiante02'),
        (1100003,'Sebastian','Cardenas','S','estudiante03'),
        (1100004,'Javier','Landazabal','A','administrativo04'),
        (1100005,'David','Castro','A','administrativo05'),
        (1100006,'Diego andres','Medina','A','administrativo06'),
        (1100007,'Sidney Karina','Villalba','D','administrador07'),
        (1100008,'Jaime Alfonso','Barragan Olarte','D','administrador08'),
        (1100009,'Carmen Alicia','Daza Gómez','D','administrador09'),
        (1100010,'Carlos Alberto','Pava','E','ejecutor10'),
        (1100011,'Melkisedeth','Tinjaka','E','ejecutor11'),
        (1100012,'Aurelio ','Gómez Gómez','E','ejecutor12');

# estado
# P = Pendiente
# A = Activa
# E = Eliminada
# INSERT INTO auxiliaturas (fecha_inicio , Administrativo_creador , estado , texto,auxiliar , fecha_finalizacion)

INSERT INTO auxiliaturas (fecha_inicio, Administrativo_creador, estado, texto, auxiliar)
VALUES (now(), 1100004 , 'A', 'Colaborar en archivo, 20 horas semanales, conocimientos en ofimatica, ultimos semestres' , 1100001);

INSERT INTO auxiliaturas (fecha_inicio , Administrativo_creador , estado , texto)
VALUES (now(), 1100005 , 'P', 'Auxiliar biblioteca,mínimo 2 horas diarias, ultimos semestres, ingreso por horas laboradas');

INSERT INTO auxiliaturas (fecha_inicio , Administrativo_creador , estado , texto, auxiliar , fecha_finalizacion)
VALUES (now(), 1100006 , 'E', 'Auxiliar archivo, conocimientos en ofimatica, ultimos semestres e ingreso monetario dependiente de las horas que se cumplan' , 1100003, now());

# A = Activa
# I = Inactiva
#INSERT INTO postulaciones (aspirante , fecha_postulacion , fecha_dada_baja , estado , auxiliatura)

INSERT INTO postulaciones (aspirante , fecha_postulacion  , estado , auxiliatura)
VALUES (1100001, now(), 'A' , 1);

INSERT INTO postulaciones (aspirante , fecha_postulacion  , fecha_dada_baja , estado , auxiliatura)
VALUES (1100003, now() , now(), 'I' , 3);