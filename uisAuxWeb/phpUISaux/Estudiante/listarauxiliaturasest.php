<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Listar Auxiliaturas</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Course Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/gif" href="../../images/icono.gif" />
<link rel="stylesheet" type="text/css" href="../../styles/bootstrap4/bootstrap.min.css">
<link href="../../plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../../styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="../../styles/responsive.css">

<?php 
include ("../../conexion.php"); 
?>
</head>
<body>

<div class="super_container">

	<!-- Header -->
	<?php
error_reporting(0); //no se reportan los notice
	if($_SESSION["usuario"] == "estudiante"){
		include('headerEstudiante.php');
		}else if ($_SESSION["usuario"] == "administrador") {
			include( '../Administrador/headerAdministrador.php') ;
		}else if ($_SESSION["usuario"] == "administrativo") {
			include('../Administrativo/headerAdministrativo.php') ;
		}else if ($_SESSION["usuario"] == "ejecutor") {
			include('../Ejecutor/headerEjecutor.php');
		}
		else{
			include('../headerVisitante.php') ;

		}
	
?>

	
	
	<!-- Home -->

	
	<!-- Popular -->

	<div class="popular page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
	</br></br>	</br><h1>Listado de Auxiliaturas</h1>
					</div>
				</div>
			</div>

			<iframe src="http://localhost/UisAux/uisAuxWeb/busquedaEstu.html" scrolling="no" frameborder="0"  width="100%" height="1000px" overflow="hidden"class="ejecjs"> </iframe>
	

			</div>
		</div>		
	</div>

	<!-- Footer -->
	<?php
include ("../footerRegistrar.php"); 
	
?>

</div>

<script src="../../js/jquery-3.2.1.min.js"></script>
<script src="../../styles/bootstrap4/popper.js"></script>
<script src="../../styles/bootstrap4/bootstrap.min.js"></script>
<script src="../../plugins/greensock/TweenMax.min.js"></script>
<script src="../../plugins/greensock/TimelineMax.min.js"></script>
<script src="../../plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="../../plugins/greensock/animation.gsap.min.js"></script>
<script src="../../plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="../../plugins/scrollTo/jquery.scrollTo.min.js"></script>
<script src="../../plugins/easing/easing.js"></script>
<script src="../../js/courses_custom.js"></script>

</body>
</html>