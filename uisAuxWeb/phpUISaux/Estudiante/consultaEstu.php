<?php 
//////CONEXION BD //////
$servername = "localhost";
$database = "uisaux";
$username = "root";
$password = "";
// Create connection
$conexion = mysqli_connect($servername, $username, $password, $database);
////// VALORES INICIALES /////// 
$tabla="";
$query="SELECT nombreaux, texto, fecha_inicio, fecha_finalizacion FROM auxiliaturas WHERE estado = 'A'";

if(isset($_POST['auxiliaturas']))
{
    $q=$conexion->real_escape_string($_POST['auxiliaturas']);
    $query="SELECT nombreaux, texto, fecha_inicio, fecha_finalizacion FROM auxiliaturas WHERE
        estado = 'A' AND( 
        nombreaux LIKE '%".$q."%' OR
        texto LIKE '%".$q."%' OR
        fecha_inicio LIKE '%".$q."%' OR
        fecha_finalizacion LIKE '%".$q."%')";
}
$buscarAuxliatura=$conexion->query($query);
if ($buscarAuxliatura->num_rows > 0)
{
    $tabla.=
    '<table class="table">
        <tr class="bg-primary">
            <td>NOMBRE AUXILIATURA</td>
            <td>DESCRIPCION</td>
            <td>INICIO</td>
            <td>FIN</td>
            <td>Postularse</td>
        </tr>';
    while($filaAuxiliaturas = $buscarAuxliatura -> fetch_assoc())
    {
        $tabla.=
        '<tr>
            <td>'.$filaAuxiliaturas['nombreaux'].'</td>
            <td>'.$filaAuxiliaturas['texto'].'</td>
            <td>'.$filaAuxiliaturas['fecha_inicio'].'</td>
            <td>'.$filaAuxiliaturas['fecha_finalizacion'].'</td>
            <td><input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1"></td>
        </tr>';
    }
    $tabla.='</table>';
}   
else
{
    $tabla="No se encontraron coincidencias con los criterios de busqueda.";
}

echo $tabla;
?>