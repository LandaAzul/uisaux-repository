import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
//import 'GestionarAux.php';
//import Navigation from './components/Navigation';
import { datos } from './datos.json';
//import { auxiliaturas } from './bd.json';
//import DatoForm from './components/DatoForm';
//para importar im archivo de la carpeta, en este caso un json
//console.log(datos);

//function App() {
  // HACK: function app parece ser que hace lo mismo que class App extends Component {
  // className son clases de css que react lo pone asi para evitar conflictos
//a continuacion se crea el App que ejecutara los otros componentes que se indiquen como etiquetas
class App extends Component {
  constructor()
  {
    super();
    //para poder heredar todas la funcionalidades del componente de react
    this.state = {
      datos
    };
    this.handleAddDato = this.handleAddDato.bind(this);
    this.handleRefuseDato = this.handleRefuseDato.bind(this);
    
  }

  handleAddDato (dato)
  {
    this.setState({
      datos: [...this.state.datos, dato]
    })
  }
  handleRefuseDato(index)
  {
    //console.log(index)
    this.setState({
      datos: this.state.datos.filter((e, i) => {
      //e de elemento
        return  i !== index
      })
    })
  }
  handleAceptarDato(index)
  {
    console.log(index)
    //aceptarAux(index)
  }

  render(){
    const datos = this.state.datos.map((dato, i) =>{
      return (
        <div className="col-md-4" key={i}>
          <div className="card mt-4">
            <div className="card-header">
              <h3>{dato.nombreaux}</h3>
              <span className="badge badge-pill badge-info ml-2">
                {dato.nombre}
              </span>
              </div>
                <div className="card-body">
                  <p>{dato.texto}</p>
                </div>
                <div className="card-footer">
                  <button
                    className="btn btn-info"
                    onClick={this.handleRefuseDato.bind(this, i)}
                    name="btnAceptar">
                    Aceptar
                  </button>
                  <button
                    className="btn btn-danger"
                    onClick={this.handleRefuseDato.bind(this, i)}
                    name="btnRechazar">
                    Rechazar
                  </button>
              </div>
          </div>
        </div>
      )
    })
    return (  //con class component tuve que agregarle el render con function app no
      <div className="App">
        <nav className="navbar navbar-dark bg-dark">
          <a href="" className="text-white">
            Auxiliaturas
            <span className='badge badge-pill badge-light ml-2'>
              {this.state.datos.length}
            </span>
            </a>
        </nav>
        <div className="container">
          <div className='row mt-4'>
            <div className='row md-3'>
            {/* <DatoForm onAddDato={this.handleAddDato}/> formulario para agregar tareas*/}
            </div>
            <div className="row mt-9">
              { datos }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
