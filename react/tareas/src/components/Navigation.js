import React, {Component} from 'react';
//al importar, el primer react es en mayus y el segundo en minus
//para llamarlo se usa
//<Navigation titulo="tarea1"/>
class Navigation extends Component {
  render() {
    return(
      <nav className="navbar navbar-dark bg-dark">
        <a href="" className="text-white">
        {this.props.ID}
        {this.props.titulo}
        {this.props.color}
        {this.props.descripcion}
        {this.props.fecha}
        {this.props.hora}
        </a>
      </nav>
    )
  }
}
export default Navigation;
