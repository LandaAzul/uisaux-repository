

const Avatar = props => <img src={props.user.img} alt={props.user.name}/>
const UserName = props => <p>{props.user.name}</p>

const User = props =>{
    return(
        <div className='user-item'>
            <Avatar user={props.user}/>
            <UserName user={props.user}/>
        </div>
    )
};
const UsersList = props => {
    const userList = props.list.map((user,i) => <User user={user} key={i} />);
    return (
        <div className='user-list'>
            {userList}
        </div>
    )
};
const user = {
    //recibe json de bd https://www.youtube.com/watch?v=ld8pG4RPAeI
    //name: 'Daniel',
    //img: 'link'
};

ReactDOM.render(<UsersList list={nombrejson}/>, document.getElementById('app'));
