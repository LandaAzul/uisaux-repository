import React , {Component} from 'react'

class DatoForm extends Component {
  constructor(){
    super();
    this.state = {
    id: '',//toca poner un autoincremento
    titulo: '',
    color: '000',
    descripcion: '',
    fecha: '',
    hora:''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    //siempre hay que llamarlos los eventos en el constructor
    //para que no pierdan el scope
}
  handleSubmit(e){
    e.preventDefault();//este evento evita refrescar la pagina
    this.props.onAddDato(this.state);
    this.setState({
      title:"",
      color: '000',
      descripcion: '',
      fecha: '',
      hora:''
    });
  }
  handleInputChange(e){
    const {value, name} = e.target;
    console.log(value, name);
    this.setState({
      [name]:value
    });
  }
  render() {
    return(
      <div className='card'>
        <form className ='card-body'onSubmit={this.handleSubmit}>
          <div className='form-group'>
            <input
              type='text'
              name='titulo'
              onChange={this.handleInputChange}
              className='form-control'
              placeholder='titulo'
              value={this.state.title}
            />
            </div>
            <div className='form-group'>
            <input
              type='text'
              name='profesor'
              onChange={this.handleInputChange}
              className='form-control'
              placeholder='profesor'
              value={this.state.profesor}
            />
          </div>
          <div className='form-group'>
            <input
              type='text'
              name='descripcion'
              className='form-control'
              onChange={this.handleInputChange}
              placeholder='descripcion...'
              value={this.state.descripcion}

            />
          </div>
          <div className='form-group'>
            <input
              type="color"
              className="form-control"
              value="#563d7c"
              onChange={this.handleInputChange}
              id="example-color-input"
              value={this.state.color}

              />
          </div>
          <button type="submit" className="btn btn-primary">
            Save
          </button>
        </form>

      </div>
    )
  }
}
export default DatoForm;
