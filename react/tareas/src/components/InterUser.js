import React , {Component} from 'react'

class saludo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            nombre: 'Landa'
        }
    }
    //funcion normal
    //despedirse(){
    //funcion flecha
    despedirse = () => {
        //alert('adios')
        this.setState({nombre:'Master'
        })
    }
    render(){
        return(
            <h1 onClick={this.despedirse}>Hola {this.state.nombre}</h1>
        )
    }
}