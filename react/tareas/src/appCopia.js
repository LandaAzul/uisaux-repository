import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
//import Navigation from './components/Navigation';
//import { datos } from './datos.json';
import { auxiliaturas } from './bd.json';
import DatoForm from './components/DatoForm'
//para importar im archivo de la carpeta, en este caso un json
//console.log(datos);

//function App() {
  // HACK: function app parece ser que hace lo mismo que class App extends Component {
  // className son clases de css que react lo pone asi para evitar conflictos
class App extends Component {
  constructor(){
    super();
    //para poder heredar todas la funcionalidades del componente de react
    this.state = {
      auxiliaturas
    };
    this.handleAddDato = this.handleAddDato.bind(this);
    this.handleRefuseDato = this.handleRefuseDato.bind(this);
  }

  handleAddDato (dato){
    this.setState({
      auxiliaturas: [...this.state.auxiliaturas, dato]
    })
  }
  handleRefuseDato(index){
    //console.log(index)
    this.setState({
      auxiliaturas: this.state.auxiliaturas.filter((e, i) => {
      //e de elemento
        return  i !== index
      })
    })
  }
  handleAceptarDato(index){
    console.log(index)
  }

  render(){
    const auxiliaturas = this.state.auxiliaturas.map((dato, i) =>{
      return (
        <div className="col-md-4" key={i}>
          <div className="card mt-4">
            <div className="card-header">
              <h3>{dato.titulo}</h3>
              <span className="badge badge-pill badge-info ml-2">
                {dato.profesor}
              </span>
              </div>
                <div className="card-body">
                  <p>{dato.descripcion}</p>
                </div>
                <div className="card-footer">
                  <button
                    className="btn btn-info"
                    onClick={this.handleAceptarDato.bind(this, i)}>
                    Aceptar
                  </button>
                  <button
                    className="btn btn-danger"
                    onClick={this.handleRefuseDato.bind(this, i)}>
                    Rechazar
                  </button>
              </div>
          </div>
        </div>
      )
    })
    return (  //con class component tuve que agregarle el render con function app no
      <div className="App">
        <nav className="navbar navbar-dark bg-dark">
          <a href="" className="text-white">
            Auxiliaturas
            <span className='badge badge-pill badge-light ml-2'>
              {this.state.auxiliaturas.length}
            </span>
            </a>
        </nav>
        <div className="container">
          <div className='row mt-4'>
            <div className='row md-3'>
              <DatoForm onAddDato={this.handleAddDato}/>
            </div>
            <div className="row mt-9">
              { auxiliaturas }
            </div>
          </div>
        </div>
      <img src={logo} className="App-logo" alt="logo" />
      </div>
    );
  }
}

export default App;